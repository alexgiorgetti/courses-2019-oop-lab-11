﻿using ComplexNumbers;
using System;

namespace _03_ComplexNumbers
{
    static class ComplexNumExtension
    {
        public static ComplexNum Invert(ComplexNum num)
        {
            return new ComplexNum((num.Re) / (Math.Pow(num.Re, 2) + Math.Pow(num.Im, 2)), (-1 * num.Im) / (Math.Pow(num.Re, 2) + Math.Pow(num.Im, 2)));
        }
    }
}
