﻿namespace Cards
{
    public class Card
    {
        public string Seed { get; }
        public string Value { get; }

        public Card(string value, string seed)
        {
            this.Value = value;
            this.Seed = seed;
        }

        public override string ToString()
        {
            // TODO comprendere il meccanismo denominato in C# "string interpolation"
            return $"{this.GetType().Name}(Name={this.Value}, Seed={this.Seed})";
        }
    }

    
}
