﻿using System;

namespace ComplexNumbers
{
    public class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.Re, 2) + Math.Pow(this.Im, 2));
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(this.Re, this.Im * -1);
            }
        }

        public static ComplexNum operator +(ComplexNum num1, ComplexNum num2)
        {
            return new ComplexNum(num1.Re + num2.Re, num1.Im + num2.Im);
        }

        public static ComplexNum operator -(ComplexNum num1, ComplexNum num2)
        {
            return new ComplexNum(num1.Re - num2.Re, num1.Im - num2.Im);
        }

        public static ComplexNum operator *(ComplexNum num1, ComplexNum num2)
        {
            return new ComplexNum(num1.Re*num2.Re - num1.Im*num2.Im, num1.Re*num2.Im + num1.Im*num2.Re);
        }

        public static ComplexNum operator /(ComplexNum num1, ComplexNum num2)
        {
            return new ComplexNum(((num1.Re * num2.Re) + (num1.Im * num2.Im)) / (Math.Pow(num2.Re, 2) + Math.Pow(num2.Im, 2)), (num1.Im*num2.Re - num1.Re*num2.Im) / (Math.Pow(num2.Re, 2) + Math.Pow(num2.Im, 2)));
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            if(this.Im < 0)
                return this.Re+" -"+Math.Abs(this.Im) + "i";
            else
                return this.Re + " +" + Math.Abs(this.Im) + "i";
        }
    }
}
