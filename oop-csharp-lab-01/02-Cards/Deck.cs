﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        {
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */

            cards = new Card[Enum.GetNames(typeof(ItalianSeed)).Length * Enum.GetNames(typeof(ItalianValue)).Length];
            int i = 0;
            foreach (ItalianSeed elem in (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed))) {
                foreach (ItalianValue elem2 in (ItalianValue[])Enum.GetValues(typeof(ItalianValue))) {
                    Card newElem = new Card(Convert.ToString(elem2), Convert.ToString(elem));
                    cards[i] = newElem;
                    i++;
                }
            }

            //throw new NotImplementedException();
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            Console.WriteLine("Stampa del mazzo di carte.\n");
            foreach (Card card in cards) {
                Console.WriteLine(card.ToString());
            }

            //throw new NotImplementedException();
        }

        public Card this[ItalianSeed seed, ItalianValue value] {
            get { return cards[Convert.ToInt32(seed) * 10 + Convert.ToInt32(value)]; }
        }

    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
